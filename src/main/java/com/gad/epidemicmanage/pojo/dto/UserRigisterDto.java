package com.gad.epidemicmanage.pojo.dto;

import lombok.Data;

@Data
public class UserRigisterDto {
    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String userPassword;

    /**
     * 确认密码
     */
    private String userAgainPasaword;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserAgainPasaword() {
        return userAgainPasaword;
    }

    public void setUserAgainPasaword(String userAgainPasaword) {
        this.userAgainPasaword = userAgainPasaword;
    }
}
