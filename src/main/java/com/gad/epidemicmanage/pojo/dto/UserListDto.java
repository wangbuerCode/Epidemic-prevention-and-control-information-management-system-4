package com.gad.epidemicmanage.pojo.dto;

import lombok.Data;

@Data
public class UserListDto {
    /**
     * 用户名称 非必填
     */
    private String userName;

    private Integer pageSize;

    private Integer currentPage;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }
}
