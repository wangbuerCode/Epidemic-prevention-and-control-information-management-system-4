package com.gad.epidemicmanage.pojo.dto;

import lombok.Data;

@Data
public class UpdatePasswdDto {
    private Integer userId;

    private String oldPassword;

    private String newPassword;

    private String newRePssword;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewRePssword() {
        return newRePssword;
    }

    public void setNewRePssword(String newRePssword) {
        this.newRePssword = newRePssword;
    }
}
