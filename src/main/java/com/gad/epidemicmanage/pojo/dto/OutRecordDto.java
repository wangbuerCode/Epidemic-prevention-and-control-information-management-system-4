package com.gad.epidemicmanage.pojo.dto;

import lombok.Data;

@Data
public class OutRecordDto {

    /**
     *主键,时间加用户id生成
     */
    private String id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 外出时间
     */
    private String outStartTime;

    /**
     * 返回时间
     */
    private String outBackTime;

    private Integer pageSize;

    private Integer currentPage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOutStartTime() {
        return outStartTime;
    }

    public void setOutStartTime(String outStartTime) {
        this.outStartTime = outStartTime;
    }

    public String getOutBackTime() {
        return outBackTime;
    }

    public void setOutBackTime(String outBackTime) {
        this.outBackTime = outBackTime;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }
}
