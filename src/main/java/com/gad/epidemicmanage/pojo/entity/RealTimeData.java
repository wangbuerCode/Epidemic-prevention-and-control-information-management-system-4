package com.gad.epidemicmanage.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("real_time_data")
public class RealTimeData implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     *主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 地区
     */
    private String place;
    /**
     * 日期
     */
    private String date;
    /**
     * 现存确诊
     */
    @TableField(value = "e_diagnosis")
    private Integer exitDiagnosis;

    /**
     * 累计确诊
     */
    @TableField(value = "c_diagnosis")
    private Integer countDiagnosis;

    /**
     * 境外输入
     */
    private Integer abroad;

    /**
     * 无症状
     */
    private Integer asymptomatic;

    /**
     * 现存疑似
     */
    @TableField(value = "e_suspected")
    private Integer exitSuspected;

    /**
     * 现存重症
     */
    @TableField(value = "e_severe")
    private Integer exitSevere;

    /**
     * 累计治愈
     */
    @TableField(value = "c_cure")
    private Integer countCure;

    /**
     * 累计死亡
     */
    @TableField(value = "c_death")
    private Integer countDeath;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getExitDiagnosis() {
        return exitDiagnosis;
    }

    public void setExitDiagnosis(Integer exitDiagnosis) {
        this.exitDiagnosis = exitDiagnosis;
    }

    public Integer getCountDiagnosis() {
        return countDiagnosis;
    }

    public void setCountDiagnosis(Integer countDiagnosis) {
        this.countDiagnosis = countDiagnosis;
    }

    public Integer getAbroad() {
        return abroad;
    }

    public void setAbroad(Integer abroad) {
        this.abroad = abroad;
    }

    public Integer getAsymptomatic() {
        return asymptomatic;
    }

    public void setAsymptomatic(Integer asymptomatic) {
        this.asymptomatic = asymptomatic;
    }

    public Integer getExitSuspected() {
        return exitSuspected;
    }

    public void setExitSuspected(Integer exitSuspected) {
        this.exitSuspected = exitSuspected;
    }

    public Integer getExitSevere() {
        return exitSevere;
    }

    public void setExitSevere(Integer exitSevere) {
        this.exitSevere = exitSevere;
    }

    public Integer getCountCure() {
        return countCure;
    }

    public void setCountCure(Integer countCure) {
        this.countCure = countCure;
    }

    public Integer getCountDeath() {
        return countDeath;
    }

    public void setCountDeath(Integer countDeath) {
        this.countDeath = countDeath;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
