package com.gad.epidemicmanage.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("out_record")
public class OutRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *主键,时间加用户id生成
     */
    @TableId(value = "id")
    private String id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 事情
     */
    private String thing;

    /**
     * 外出时间
     */
    private String outStartTime;

    /**
     * 返回时间
     */
    private String outBackTime;

    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getThing() {
        return thing;
    }

    public void setThing(String thing) {
        this.thing = thing;
    }

    public String getOutStartTime() {
        return outStartTime;
    }

    public void setOutStartTime(String outStartTime) {
        this.outStartTime = outStartTime;
    }

    public String getOutBackTime() {
        return outBackTime;
    }

    public void setOutBackTime(String outBackTime) {
        this.outBackTime = outBackTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
