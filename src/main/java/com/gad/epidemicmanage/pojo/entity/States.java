package com.gad.epidemicmanage.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("states")
public class States implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户账户名
     */
    private String userName;

    /**
     * 是否异常
     */
    @TableField(value = "is_abnormal")
    private Integer abnormal;

    /**
     * 是否高风险地区
     */
    @TableField(value = "is_high_risk")
    private Integer highRisk;

    /**
     * 隔离天数
     */
    private Integer homeQuarantineDay;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAbnormal() {
        return abnormal;
    }

    public void setAbnormal(Integer abnormal) {
        this.abnormal = abnormal;
    }

    public Integer getHighRisk() {
        return highRisk;
    }

    public void setHighRisk(Integer highRisk) {
        this.highRisk = highRisk;
    }

    public Integer getHomeQuarantineDay() {
        return homeQuarantineDay;
    }

    public void setHomeQuarantineDay(Integer homeQuarantineDay) {
        this.homeQuarantineDay = homeQuarantineDay;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
